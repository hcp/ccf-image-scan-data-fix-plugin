/*!
 * XNAT Task site settings functions
 */
var XNAT = getObject(XNAT);

(function(factory) {
    if (typeof define === 'function' && define.amd) {
        define(factory);
    } else if (typeof exports === 'object') {
        module.exports = factory();
    } else {
        return factory();
    }
}(function() {

    var ccfImageScanDataFixConf;
    XNAT.admin =
        getObject(XNAT.admin);

    XNAT.admin.ccfImageScanDataFixConf = ccfImageScanDataFixConf =
        getObject(XNAT.admin.ccfImageScanDataFixConf);

    ccfImageScanDataFixConf.downloadData = function() {
        XNAT.xhr.get({
            url: XNAT.url.restUrl('/xapi/imagescanDataIssues/downloadConflictedDataToCache'),
            success: function(data) {
                XNAT.ui.banner.top(2000, 'Downloaded Data to cache location.', 'success');
            },
            error: function() {
                XNAT.ui.banner.top(3000, '<b>Unable to download data to cache. Kindly check logs for errors.', 'error', '600px');
            }
        })

    }

    ccfImageScanDataFixConf.resolveData = function() {
        XNAT.xhr.post({
            url: XNAT.url.restUrl('/xapi/imagescanDataIssues/resolveConflicts'),
            success: function(data) {
                XNAT.ui.banner.top(2000, data, 'success');
				ccfImageScanDataFixConf.clearConflictedDataTable();
				ccfImageScanDataFixConf.getConflictedDataCounts();
            },
            error: function() {
                XNAT.ui.banner.top(3000, '<b>Unable to update conflicted records. Kindly check logs for errors.', 'error', '600px');
            }
        })
    }

    ccfImageScanDataFixConf.getConflictedDataCounts = function() {
        ccfImageScanDataFixConf.clearConflictedDataTable();
		ccfImageScanDataFixConf.hideResolveButton();
        XNAT.xhr.get({
            url: XNAT.url.restUrl('/xapi/imagescanDataIssues/getConflictCounts'),
            success: function(data) {
                document.getElementById("conflictCount").value = data;
                XNAT.ui.banner.top(2000, data + ' conflicted records present in the database.', 'success');
            },
            error: function() {
                XNAT.ui.banner.top(3000, '<b>Unable to pull counts from database. Kindly check logs for errors.', 'error', '600px');
            }
        })
    }

    ccfImageScanDataFixConf.showData = function() {
        ccfImageScanDataFixConf.clearConflictedDataTable();
        var conflictTable = XNAT.table({
            className: 'xnat-table compact',
            style: {
                width: '100%'
			}
        });
        conflictTable.tr();
        conflictTable
            .th('Project')
            .th('Subject')
            .th('Label')
            .th('Experiment')
            .th('Session')
            .th('XNAT Image Scan Data Id')
            .th('Type')
            .th('Extension');
        ccfImageScanDataFixConf.result(conflictTable);
		ccfImageScanDataFixConf.getConflictedDataCounts();
    }

    ccfImageScanDataFixConf.clearConflictedDataTable= function() {
        $("#conflicted-data-table").empty();
    }
	
	ccfImageScanDataFixConf.hideResolveButton= function() {
        document.getElementById('resolveData').style.display='none';
    }
	
	ccfImageScanDataFixConf.showResolveButton= function() {
        document.getElementById('resolveData').style.display='block';
    }
	

    ccfImageScanDataFixConf.result= function(conflictTable) {
        XNAT.xhr.get({
            url: XNAT.url.restUrl('/xapi/imagescanDataIssues/showConflictedData'),
            success: function(data) {
                XNAT.ui.banner.top(2000, 'Loading Conflicted data.', 'success');
                var allRows = data.map(function(item, i) {

                    return [
                        item.project,
                        item.subject, item.label, item.experiment, item.session, item.xnat_imagescandata_id, item.type, item.extension
                    ];
                });
                conflictTable.rows(allRows);

                if (data.length) {
                    $("#conflicted-data-table").append(conflictTable.table);
					$("#conflicted-data-table").find("tr:odd").css("background-color", "##F2F2F2");
					$("#conflicted-data-table").find("tr:even").css("background-color", "#D6D6D6");
					ccfImageScanDataFixConf.showResolveButton();
                } else {
                    $("#conflicted-data-table").spawn('p', '<b>There is no conflicted data in the database.</b>')
                }
            },
            error: function() {
                XNAT.ui.banner.top(3000, '<b>Unable to load data. Kindly check logs for errors.', 'error', '600px');
            }
        })
    }

    XNAT.admin.ccfImageScanDataFixConf = ccfImageScanDataFixConf;
}));