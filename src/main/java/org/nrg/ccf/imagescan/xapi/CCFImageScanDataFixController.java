/**
 * 
 */
package org.nrg.ccf.imagescan.xapi;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nrg.ccf.imagescan.utils.QueryService;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.xapi.exceptions.InsufficientPrivilegesException;
import org.nrg.xdat.rest.AbstractXapiRestController;
import org.nrg.xdat.security.helpers.Roles;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.nrg.xft.security.UserI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * The Class CCFImageScanDataFixController.
 */
@SuppressWarnings("deprecation")
@XapiRestController
@RequestMapping(value = "/imagescanDataIssues")
@Api(description = "CCF Image Data Fix Configuration API")
public class CCFImageScanDataFixController extends AbstractXapiRestController {

	/** The logger. */
	public static Logger logger = Logger.getLogger(CCFImageScanDataFixController.class);

	/** The query svc. */
	QueryService querySvc = new QueryService();

	/**
	 * Instantiates a new CCF image scan data fix controller.
	 *
	 * @param userManagementService
	 *            the user management service
	 * @param roleHolder
	 *            the role holder
	 */
	@Autowired
	protected CCFImageScanDataFixController(UserManagementServiceI userManagementService, RoleHolder roleHolder) {
		super(userManagementService, roleHolder);
	}

	/**
	 * Gets the other dicom and image scan conflict counts.
	 *
	 * @return the other dicom and image scan conflict counts
	 */
	@ApiOperation(value = "Returns Image and otherdicom scan data conflicts counts", response = String.class)
	@ApiResponses({@ApiResponse(code = 200, message = "Successfully fetched the count for conflicted records."),
        @ApiResponse(code = 403, message = "Insufficient permissions to get the count value."),
        @ApiResponse(code = 500, message = "An unexpected error occurred.")})
	@RequestMapping(value = "/getConflictCounts", produces = MediaType.TEXT_PLAIN_VALUE, method = RequestMethod.GET)
	public ResponseEntity<String> getOtherDicomAndImageScanConflictCounts() {
		final UserI user = getSessionUser();
		try {
			if (Roles.isSiteAdmin(user)) {
				return new ResponseEntity<String>(querySvc.getOtherDicomAndImageScanConflictCounts().toString(),
						HttpStatus.OK);
			} else {
				throw new InsufficientPrivilegesException(user.getUsername());
			}
		} catch (Exception e) {
			return getErrorMessage(e);
		}
	}

	/**
	 * Update other dicom extension in image scan.
	 *
	 * @return the response entity
	 */
	@ApiOperation(value = "Resolve Image and otherdicom scan data conflicts", response = String.class)
	@ApiResponses({@ApiResponse(code = 200, message = "Successfully updated conflicted data."),
        @ApiResponse(code = 403, message = "Insufficient permissions to update data."),
        @ApiResponse(code = 500, message = "An unexpected error occurred.")})
	@RequestMapping(value = "/resolveConflicts", produces = MediaType.TEXT_PLAIN_VALUE, method = RequestMethod.POST)
	public ResponseEntity<String> updateOtherDicomExtensionInImageScan() {
		final UserI user = getSessionUser();
		logger.info(user.getUsername() +" tried to resolve data on "+new SimpleDateFormat("MM_dd_yyyy_hh_mm_ss").format(new Date()));
		try {
			if (Roles.isSiteAdmin(user)) {
				//getOtherDicomAndImageScanConflictDetailedData API will get all the conflicted records and dump them into a file under cache directory.
				querySvc.getOtherDicomAndImageScanConflictDetailedData();
				return new ResponseEntity<String>(String.format("%d records updated in the database.",
						querySvc.updateOtherDicomExtensionInImageScan()), HttpStatus.OK);
			} else {
				logger.info("Operation failed due to insufficient privileges.");
				throw new InsufficientPrivilegesException(user.getUsername());
			}
		} catch (Exception e) {
			return getErrorMessage(e);
		}
	}

	/**
	 * Gets the other dicom and image scan conflict detailed data.
	 *
	 * @return the other dicom and image scan conflict detailed data
	 */
	@ApiOperation(value = "Downloads Image and otherdicom scan conflicted records to cache location", response = String.class)
	@ApiResponses({@ApiResponse(code = 200, message = "Successfully downloaded conflicted data details to cache."),
        @ApiResponse(code = 403, message = "Insufficient permissions to download data."),
        @ApiResponse(code = 500, message = "An unexpected error occurred.")})
	@RequestMapping(value = "/downloadConflictedDataToCache", produces = MediaType.TEXT_PLAIN_VALUE, method = RequestMethod.GET)
	public ResponseEntity<String> downloadConflictedDataToCache() {
		final UserI user = getSessionUser();
		try {
			if (Roles.isSiteAdmin(user)) {
				querySvc.getOtherDicomAndImageScanConflictDetailedData();
				return new ResponseEntity<String>(HttpStatus.OK);
			} else {
				throw new InsufficientPrivilegesException(user.getUsername());
			}
		} catch (Exception e) {
			return getErrorMessage(e);
		}
	}

	@ApiOperation(value = "Downloads Image and otherdicom scan conflicted records to cache location", response = String.class)
	@ApiResponses({@ApiResponse(code = 200, message = "Successfully downloaded conflicted data details to cache."),
        @ApiResponse(code = 403, message = "Insufficient permissions to download data."),
        @ApiResponse(code = 500, message = "An unexpected error occurred.")})
	@RequestMapping(value = "/showConflictedData", method = RequestMethod.GET)
	public ResponseEntity<List<Map<String, Object>>> showConflictedData() {
		final UserI user = getSessionUser();
		try {
			if (Roles.isSiteAdmin(user)) {
				return new ResponseEntity<List<Map<String, Object>>>(querySvc.getOtherDicomAndImageScanConflictDetailedDataAsJSON(user),HttpStatus.OK);
			} else {
				throw new InsufficientPrivilegesException(user.getUsername());
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Gets the error message.
	 *
	 * @param e the e
	 * @return the error message
	 */
	private ResponseEntity<String> getErrorMessage(Exception e) {
		logger.error(e);
		e.printStackTrace();
		if(e instanceof InsufficientPrivilegesException)
			return new ResponseEntity<>("ERROR: "+e.getMessage(), HttpStatus.FORBIDDEN);
		else
			return new ResponseEntity<>("ERROR: "+e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
