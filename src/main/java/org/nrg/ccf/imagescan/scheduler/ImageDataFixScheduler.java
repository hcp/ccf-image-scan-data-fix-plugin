/**
 * 
 */
package org.nrg.ccf.imagescan.scheduler;

import java.io.IOException;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.nrg.ccf.imagescan.utils.QueryService;
import org.nrg.xft.exception.DBPoolException;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * The Class ImageDataFixScheduler is a scheduled job class which will run after every
 * 30 minutes and fix otherdicom scan data. 
 * We are not using XNAT scheduled service/framework for this because this class 
 * is not referring any XNAT service. Business logic is making JDBC calls to check and fix conflicts.
 * 
 */
@Configuration
@EnableScheduling
public class ImageDataFixScheduler {
	/** The logger. */
	public static Logger logger = Logger.getLogger(ImageDataFixScheduler.class);

	/**
	 * This API will check and fix conflicted records. This is a scheduled job which
	 * will run after every 30 minutes.
	 */
	@Scheduled(fixedDelay = 1000 * 60 * 30)
	public void fixData() {
		QueryService qs = new QueryService();
		try {
			qs.getOtherDicomAndImageScanConflictDetailedData();
			qs.updateOtherDicomExtensionInImageScan();
		} catch (SQLException e) {
			e.printStackTrace();
			logger.error(e);
		} catch (IOException e) {
			e.printStackTrace();
			logger.error(e);
		} catch (DBPoolException e) {
			e.printStackTrace();
			logger.error(e);
		}
	}
}
