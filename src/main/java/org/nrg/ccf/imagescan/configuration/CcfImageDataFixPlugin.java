/**
 * 
 */
package org.nrg.ccf.imagescan.configuration;

import org.apache.log4j.Logger;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

// TODO: Auto-generated Javadoc
/**
 * The Class CcfImageDataFixPlugin.
 *
 * @author Atul
 */
@XnatPlugin(value = "ccfImageDataFixPlugin", name = "CCF Image Data Fix Plugin")
@ComponentScan({ "org.nrg.ccf.imagescan.xapi", "org.nrg.ccf.imagescan.utils","org.nrg.ccf.imagescan.scheduler" })
public class CcfImageDataFixPlugin {
	/** The logger. */
	public static Logger logger = Logger.getLogger(CcfImageDataFixPlugin.class);

	/**
	 * Instantiates a new ccf subject ids plugin.
	 */
	public CcfImageDataFixPlugin() {
		logger.info("Configuring CCF Image Scan Data fix plugin");
	}

}
