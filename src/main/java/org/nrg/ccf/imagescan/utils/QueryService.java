/**
 * 
 */
package org.nrg.ccf.imagescan.utils;

import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.nrg.xft.db.PoolDBUtils;
import org.nrg.xft.exception.DBPoolException;
import org.nrg.xft.security.UserI;

/**
 * The Class QueryService.
 *
 * @author Atul
 */
public class QueryService {

	/** The Constant IMAGESCAN_DATA_BACKUP_SQL. */
	private static final String IMAGESCAN_DATA_BACKUP_SQL = "imagescan.data.backup.sql";

	/** The Constant OTHERDICOM_AND_IMAGESCAN_CONFLICT_COUNTS. */
	private static final String OTHERDICOM_AND_IMAGESCAN_CONFLICT_COUNTS = "otherdicom.and.imagescan.conflict.counts";

	/** The Constant UPDATE_OTHER_DICOM_EXTENSION_IN_IMAGESCAN. */
	private static final String UPDATE_OTHER_DICOM_EXTENSION_IN_IMAGESCAN = "update.otherDicom.extension.in.imagescan";

	/** The Constant OTHERDICOM_AND_IMAGESCAN_CONFLICT_DETAILED_DATA. */
	private static final String OTHERDICOM_AND_IMAGESCAN_CONFLICT_DETAILED_DATA = "otherdicom.and.imagescan.conflict.detailed.data";

	/** The con. */
	private PoolDBUtils con = null;

	/** PreparedStatement to get other dicom and image scan conflict counts. */
	private PreparedStatement getOtherDicomAndImageScanConflictCounts = null;

	/** PreparedStatement to update other dicom extension in image scan. */
	private PreparedStatement updateOtherDicomExtensionInImageScan = null;

	/**
	 * PreparedStatement to get other dicom and image scan conflict detailed data.
	 */
	private PreparedStatement getOtherDicomAndImageScanConflictDetailedData = null;

	/** The create back up table for xnat image scan data. */
	private PreparedStatement createBackUpTableForXnatImageScanData = null;

	/**
	 * Gets the other dicom and image scan conflict counts.
	 *
	 * @return the other dicom and image scan conflict counts
	 * @throws SQLException
	 *             the SQL exception
	 * @throws DBPoolException
	 *             the DB pool exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public Integer getOtherDicomAndImageScanConflictCounts() throws SQLException, DBPoolException, IOException {
		con = new PoolDBUtils();
		ResultSet rs=null;
		int count = 0;
		try {
			getOtherDicomAndImageScanConflictCounts = con.getPreparedStatement(null,
					ImageDataFixUtils.getValue(OTHERDICOM_AND_IMAGESCAN_CONFLICT_COUNTS));
			rs = getOtherDicomAndImageScanConflictCounts.executeQuery();
			if (rs != null) {
				rs.next();
				count = rs.getInt(1);
			}
		} finally {
			closeConnection(rs);
		}
		return count;
	}

	/**
	 * Update other dicom extension in image scan.
	 *
	 * @return the integer
	 * @throws SQLException
	 *             the SQL exception
	 * @throws DBPoolException
	 *             the DB pool exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public synchronized Integer updateOtherDicomExtensionInImageScan()
			throws SQLException, DBPoolException, IOException {
		int result = 0;
		try {
			con = new PoolDBUtils();
			updateOtherDicomExtensionInImageScan = con.getPreparedStatement(null,
					ImageDataFixUtils.getValue(UPDATE_OTHER_DICOM_EXTENSION_IN_IMAGESCAN));

			//Commented createBackUpTable() because we will setup a scheduled job which will execute after every 30 mins.
			//createBackUpTable();
			result = updateOtherDicomExtensionInImageScan.executeUpdate();
		} finally {
			closeConnection(null);
		}
		return result;
	}

	/**
	 * Creates the back up table.
	 *
	 * @throws SQLException
	 *             the SQL exception
	 * @throws DBPoolException
	 *             the DB pool exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private void createBackUpTable() throws SQLException, DBPoolException, IOException {
		createBackUpTableForXnatImageScanData = con.getPreparedStatement(null,
				String.format(ImageDataFixUtils.getValue(IMAGESCAN_DATA_BACKUP_SQL),
						new SimpleDateFormat("MMM_dd_yyyy_hh_mm_ss").format(new Date())));
		createBackUpTableForXnatImageScanData.execute();

	}

	/**
	 * Gets the other dicom and image scan conflict detailed data.
	 *
	 * @return the other dicom and image scan conflict detailed data
	 * @throws SQLException
	 *             the SQL exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws DBPoolException
	 *             the DB pool exception
	 */
	public File getOtherDicomAndImageScanConflictDetailedData() throws SQLException, IOException, DBPoolException {
		File result = null;
		ResultSet rs = null;
		try {
			con = new PoolDBUtils();
			getOtherDicomAndImageScanConflictDetailedData = con.getPreparedStatement(null,
					ImageDataFixUtils.getValue(OTHERDICOM_AND_IMAGESCAN_CONFLICT_DETAILED_DATA));

			rs = getOtherDicomAndImageScanConflictDetailedData.executeQuery();
			if (rs != null && rs.next()) {
				result = new File(ImageDataFixUtils.generateFileName("OtherDicomScanDataIssue"));
				ImageDataFixUtils.generateCSVFromResultSet(rs, result);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		} finally {
			closeConnection(rs);
		}
		return result;
	}

	/**
	 * Gets the other dicom and image scan conflict detailed data as JSON.
	 *
	 * @param user
	 *            the user
	 * @return the other dicom and image scan conflict detailed data as JSON
	 * @throws SQLException
	 *             the SQL exception
	 * @throws DBPoolException
	 *             the DB pool exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public List<Map<String, Object>> getOtherDicomAndImageScanConflictDetailedDataAsJSON(UserI user)
			throws SQLException, DBPoolException, IOException {
		List<Map<String, Object>> listOfMaps = null;
		con = new PoolDBUtils();
		try {
			getOtherDicomAndImageScanConflictDetailedData = con.getPreparedStatement(null,
					ImageDataFixUtils.getValue(OTHERDICOM_AND_IMAGESCAN_CONFLICT_DETAILED_DATA));

			QueryRunner queryRunner = new QueryRunner();
			listOfMaps = queryRunner.query(getOtherDicomAndImageScanConflictDetailedData.getConnection(),
					ImageDataFixUtils.getValue(OTHERDICOM_AND_IMAGESCAN_CONFLICT_DETAILED_DATA), new MapListHandler());
		} catch (SQLException | DBPoolException | IOException e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (getOtherDicomAndImageScanConflictDetailedData != null) {
				getOtherDicomAndImageScanConflictDetailedData.close();
			}
			closeConnection(null);
		}
		return listOfMaps;
	}

	/**
	 * Close connection.
	 *
	 * @param rs
	 *            the ResultSet Object
	 */
	private void closeConnection(ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
		con.closeConnection();
	}
}
