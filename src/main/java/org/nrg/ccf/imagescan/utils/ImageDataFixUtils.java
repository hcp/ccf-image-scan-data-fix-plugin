/**
 * 
 */
package org.nrg.ccf.imagescan.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.nrg.xnat.turbine.utils.ArcSpecManager;

import au.com.bytecode.opencsv.CSVWriter;

/**
 * The Class ImageDataFixUtils.
 *
 * @author Atul
 */
public class ImageDataFixUtils {
	/**
	 * 
	 */
	private static final String QUERIES_PROPERTIES_FILE = "queries.properties";
	/**
	 * 
	 */
	private static final String CSV_FILE_EXTENSION = ".csv";
	/**
	 * 
	 */
	private static final String DATE_FORMAT_FOR_BKP_TABLE_NAME = "MM_dd_yyyy_hh_mm_ss";
	/**
	 * 
	 */
	private static final String CCF_IMAGE_SCAN_DATA_FIX_DIR = "ccfImageScanDataFixDir";
	/** The properties. */
	private static Properties properties = null;

	/**
	 * Loads the properties file.
	 *
	 * @param fileName
	 *            the file Name.
	 * @throws IOException
	 *             the exception
	 */
	private static void init(String fileName) throws IOException {

		properties = new Properties();
		properties.load(ImageDataFixUtils.class.getClassLoader().getResourceAsStream(fileName));

	}

	/**
	 * Gets the value.
	 *
	 * @param propertyName
	 *            the property name
	 * @return the value
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static String getValue(String propertyName) throws IOException {
		if (properties == null) {
			init(QUERIES_PROPERTIES_FILE);
		}
		return (String) properties.get(propertyName);
	}

	/**
	 * Generate CSV from result set.
	 *
	 * @param rs
	 *            the rs
	 * @param file
	 *            the file
	 * @throws SQLException
	 *             the SQL exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void generateCSVFromResultSet(ResultSet rs, File file) throws SQLException, IOException {
		Writer wr = new FileWriter(file);
		CSVWriter csvWriter = new CSVWriter(wr);
		csvWriter.writeAll(rs, true);
		csvWriter.close();
		wr.close();
	}

	/**
	 * Generate file name.
	 *
	 * @param prefix
	 *            the prefix
	 * @return the string
	 */
	public static String generateFileName(String prefix) {
		if (prefix == null) {
			prefix = "XNAT";
		}
		String tempDir = ArcSpecManager.GetFreshInstance().getGlobalCachePath();
		File ccfImageScanDataFixDir = new File(tempDir + File.separator + CCF_IMAGE_SCAN_DATA_FIX_DIR);
		if (!ccfImageScanDataFixDir.exists())
			ccfImageScanDataFixDir.mkdir();

		SimpleDateFormat sf = new SimpleDateFormat(DATE_FORMAT_FOR_BKP_TABLE_NAME);
		return new StringBuilder(ccfImageScanDataFixDir.getAbsolutePath()).append(File.separator).append(prefix)
				.append("_").append(sf.format(new Date())).append(CSV_FILE_EXTENSION).toString();
	}
}
